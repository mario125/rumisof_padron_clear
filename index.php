<?php

//CORRER POR EL TERMINAL: iconv -f iso-8859-1 -t utf-8 padron_reducido_ruc.txt > utf8_padron_reducido_ruc.txt
//EJECUTAR EL index.php POR EL TERMINAL:php -f archivo.php
//CABECERA PARA IMPORTAR EL *.CSV: ruc|razon|estado|domicilio|ubigeo|via|nombre_via|codigo_zona|tipo_zona|numero|interior|lote|departamento|manzana|kilometro|

echo "Limpiar caracteres especiales\n";
$data = file_get_contents('utf8_padron_reducido_ruc.txt');
$data = str_replace(array('\'', '"', '-'), '', $data);
file_put_contents('utf8_padron_reducido_ruc.txt', $data);

echo "Limpiar caracteres especiales: LISTO\n";

$replace = array('Š' => 'S', 'š' => 's', 'Ž' => 'Z', 'ž' => 'z', 'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'A', 'Ç' => 'C', 'È' => 'E', 'É' => 'E',
    'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I', 'Ñ' => 'Ñ', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ø' => 'O', 'Ù' => 'U',
    'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ý' => 'Y', 'Þ' => 'B', 'ß' => 'Ss', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'a', 'ç' => 'c',
    'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'ð' => 'o', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o',
    'ö' => 'o', 'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ý' => 'y', 'þ' => 'b', 'ÿ' => 'y');


echo "Generando output.csv\n";
$i = 0;
if (($handle1 = fopen("utf8_padron_reducido_ruc.txt", "r")) !== FALSE) {
    if (($handle2 = fopen("output.csv", "w")) !== FALSE) {
        while (($data = fgetcsv($handle1, 000, "|")) !== FALSE) {

            $i++;
            if ($i == 1) {
                continue;
            }

            // mostrar mensaje cada 100K
            if (($i % 100000) == 0) {
                echo $i . "\n";
            }

            // Solo mantener RUCs activos
            if (trim($data['2']) != 'ACTIVO') {
                continue;
            }

            $data = array_pad($data, 15, '');

            $data[0] = strtr($data[0], $replace);
            $data[1] = strtr($data[1], $replace);
            $data[2] = strtr($data[2], $replace);
            $data[3] = strtr($data[3], $replace);
            $data[4] = strtr($data[4], $replace);
            $data[5] = strtr($data[5], $replace);
            $data[6] = strtr($data[6], $replace);
            $data[7] = strtr($data[7], $replace);
            $data[8] = strtr($data[8], $replace);
            $data[9] = strtr($data[9], $replace);
            $data[10] = strtr($data[10], $replace);
            $data[11] = strtr($data[11], $replace);
            $data[12] = strtr($data[12], $replace);
            $data[13] = strtr($data[13], $replace);
            $data[14] = strtr($data[14], $replace);

            $data = array_slice($data, 0, 15);

            fputcsv($handle2, $data, '|');
        }
        fclose($handle2);
    }
    fclose($handle1);
}

echo "Generando output.csv: LISTO\n";
$data = file_get_contents('output.csv');
echo "Limpiando output.csv\n";
$data = str_replace(array('\'', '"'), '', $data);
file_put_contents('output.csv', $data);
echo "Limpiando output.csv: LISTO\n";



//  CREATE TABLE `ruc` (
//   `ruc` varchar(15) NOT NULL,
//   `razon` varchar(255) DEFAULT NULL,
//   `estado` varchar(25) DEFAULT NULL,
//   `domicilio` varchar(25) DEFAULT NULL,
//   `ubigeo` varchar(6) DEFAULT NULL,
//   `via` varchar(150) DEFAULT NULL,
//   `nombre_via` varchar(150) DEFAULT NULL,
//   `codigo_zona` varchar(150) DEFAULT NULL,
//   `tipo_zona` varchar(150) DEFAULT NULL,
//   `numero` varchar(100) DEFAULT NULL,
//   `interior` varchar(50) DEFAULT NULL,
//   `lote` varchar(50) DEFAULT NULL,
//   `departamento` varchar(50) DEFAULT NULL,
//   `manzana` varchar(50) DEFAULT NULL,
//   `kilometro` varchar(50) DEFAULT NULL,
//   `Column16` varchar(1) DEFAULT NULL
// ) ENGINE=InnoDB DEFAULT CHARSET=utf8
 
 